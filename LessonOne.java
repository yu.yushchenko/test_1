//package practice2;



public class LessonOne {


    public static void main(String[] args)  {
        int v = task1(-4);

        System.out.println(v);
    }

    public static int task1(int value) {

        if (value < 0) {
            throw new com.epam.test.automation.java.practice2.IllegalArgumentException()
        }


        int  sum = 0;
        while (value > 0)
        {
            int digit = value % 10;
            if (digit % 2 != 0)
            {
                sum += digit;
            }
            value /= 10;
        }
        return sum;
    }


    public static int task2(int value) {
        int result = 0;
        while (value > 0)
        {
            result += value % 2;
            value /= 2;
        }
        return result;
    }


    public static int task3(int value) {
       if (value <=1) {
           return 0;
       }

       
        if (value == 0) {
            throw new IllegalArgumentException("value should not be 0");
        }

        int firstNum;
        firstNum = 0;
        int secondNum = 1;
        int sum = 1;

        for (int i = 2; i < value; i++) {
            int nextNum = firstNum + secondNum;
            firstNum = secondNum;
            secondNum = nextNum;

            sum += nextNum;
        }

       return sum;
    }
}

